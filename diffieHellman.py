#OPGELET: gebruik nooit zulke kleine getallen in een productieomgeving
#Gebruikt door Alice en Bob
modulus = 557#p
base = 35 #g

#private sleutel van Alice en op basis van modulus, base en private sleutel haar eigen publieke sleutel berekenen
a = 984
A = base^a % modulus

#private sleutel van Bob en op basis van modulus, base en private sleutel zijn eigen publieke sleutel berekenen
b = 355
B = base^b % modulus

#geef de publieke sleutels van Alice en Bob weer
print("De publieke sleutel van Alice: ", B)
print("De private sleutel van Bob: ", A)

#bereken nu de geheime sleutel (dit zijn voor zowel Alice en Bob dezelfde)
sA = B^a % modulus
sB = A^b % modulus

s = sA

#Geef de berekende geheime sleutels weer, dit zouden normaal gezien dezelfde moeten zijn
print("De geheime sleutel (berekening van Alice): ", sA)
print("De geheime sleutel (berekening van Bob: ", sB)