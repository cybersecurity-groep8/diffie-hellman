import socket
import time

HOST = '127.0.0.1'  # Hostname op ip adres van 
PORT = 8080        # Poort van de server

base = str(35) #g
private_key = 984
modulus = str(557)#p

#stuur modulus naar de server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send(modulus.encode())
s.close()

#stuur base naar server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send(base.encode())

#bereken de publieke sleutel van de client
public_key = int(base)^int(private_key) % int(modulus)

#stuur de publieke sleutel van de client naar de server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send(str(public_key).encode())

#ontvang de publieke sleutel van de server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
server_public_key = s.recv(1024).decode()
if not server_public_key:
    s.close()

#bereken de gedeelde geheime sleutel. Met deze sleutel kunnen we vervolgens berichten gaan encrypteren met bijvoorbeeld DES of AES
secret = int(server_public_key) ^ int(private_key) % int(modulus)


print(f"base = {base}")
print(f"modulus = {modulus}")
print(f"server_public_key = {server_public_key}")
print(f"secret = {secret}")