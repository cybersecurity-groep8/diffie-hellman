import socket

HOST = '0.0.0.0' #ip adres waar de server op luistert. 0.0.0.0 betekend dat de server luistert op alle ip addressen
PORT = 8080 # poort waar de server op luistert
private_key = 355

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(5)
print(f"Server now listening on {HOST} port {PORT}")
base = ''
modulus = ''
client_public_key = ''
secret = ''

#ontvang modulus van client
clientsocket, address = s.accept()
print(f"Client connected from {address}")
modulus = int(clientsocket.recv(1024).decode())
if not modulus:
    s.close()

#ontvang base van client
clientsocket, address = s.accept()
print(f"Client connected from {address}")
base = int(clientsocket.recv(1024).decode())
if not base:
    s.close()

#bereken de publieke sleutel van de server
public_key = int(base)^int(private_key) % int(modulus)

#ontvang de publieke sleutel van de client
clientsocket, address = s.accept()
print(f"Client connected from {address}")
client_public_key = int(clientsocket.recv(1024).decode())
if not client_public_key:
    s.close()

#stuur de publieke sleutel van de server naar de client
clientsocket, address = s.accept()
print(f"Client connected from {address}")
clientsocket.send(str(public_key).encode())

#bereken de gedeelde geheime sleutel. Met deze sleutel kunnen we vervolgens berichten gaan encrypteren met bijvoorbeeld DES of AES
secret = int(client_public_key) ^ int(private_key) % int(modulus)

print(f"base = {base}")
print(f"modulus = {modulus}")
print(f"client_public_key = {client_public_key}")
print(f"secret = {secret}")